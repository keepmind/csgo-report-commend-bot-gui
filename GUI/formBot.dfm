object Form1: TForm1
  Left = 347
  Top = 258
  BorderStyle = bsDialog
  Caption = 'CSGO Commend And Report BOT'
  ClientHeight = 216
  ClientWidth = 289
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object pgctab: TPageControl
    Left = 8
    Top = 8
    Width = 273
    Height = 201
    ActivePage = ts1
    TabOrder = 0
    object ts1: TTabSheet
      Caption = 'Report Bot'
      object lb1: TLabel
        Left = 8
        Top = 56
        Width = 102
        Height = 13
        Caption = 'Share ID or MatchID:'
      end
      object lb2: TLabel
        Left = 8
        Top = 8
        Width = 57
        Height = 13
        Caption = 'SteamID64:'
      end
      object edt1: TEdit
        Left = 8
        Top = 72
        Width = 249
        Height = 21
        TabOrder = 0
        OnDblClick = edt1DblClick
      end
      object edt2: TEdit
        Left = 8
        Top = 24
        Width = 249
        Height = 21
        TabOrder = 1
      end
      object btn1: TButton
        Left = 24
        Top = 112
        Width = 209
        Height = 49
        Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1041#1054#1058#1072
        TabOrder = 2
        OnClick = btn1Click
      end
      object chk1: TCheckBox
        Left = 80
        Top = 0
        Width = 185
        Height = 17
        Caption = #1047#1072#1088#1077#1087#1086#1088#1090#1080#1090#1100' '#1087#1086' '#1089#1087#1080#1089#1082#1091' List.txt'
        TabOrder = 3
        OnClick = chk1Click
      end
    end
    object ts2: TTabSheet
      Caption = 'Commend Bot'
      ImageIndex = 1
      object lb3: TLabel
        Left = 8
        Top = 8
        Width = 57
        Height = 13
        Caption = 'SteamID64:'
      end
      object lbl1: TLabel
        Left = 8
        Top = 56
        Width = 102
        Height = 13
        Caption = 'Share ID or MatchID:'
      end
      object edt3: TEdit
        Left = 8
        Top = 24
        Width = 249
        Height = 21
        TabOrder = 0
      end
      object btn2: TButton
        Left = 24
        Top = 112
        Width = 209
        Height = 49
        Caption = #1047#1072#1087#1091#1089#1090#1080#1090#1100' '#1041#1054#1058#1072
        TabOrder = 1
        OnClick = btn2Click
      end
      object edt4: TEdit
        Left = 8
        Top = 72
        Width = 249
        Height = 21
        TabOrder = 2
      end
    end
  end
  object btn3: TButton
    Left = 24
    Top = 88
    Width = 241
    Height = 49
    Caption = #1059#1089#1090#1072#1085#1086#1074#1080#1090#1100' '#1079#1072#1074#1080#1089#1080#1084#1086#1089#1090#1080
    TabOrder = 1
    Visible = False
    OnClick = btn3Click
  end
  object xpmnfst1: TXPManifest
    Left = 152
  end
end
