unit formBot;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, TabNotBk, StdCtrls,ShellAPI, XPMan;

type
  TForm1 = class(TForm)
    pgctab: TPageControl;
    ts1: TTabSheet;
    ts2: TTabSheet;
    edt1: TEdit;
    lb1: TLabel;
    lb2: TLabel;
    edt2: TEdit;
    btn1: TButton;
    lb3: TLabel;
    edt3: TEdit;
    btn2: TButton;
    btn3: TButton;
    xpmnfst1: TXPManifest;
    chk1: TCheckBox;
    lbl1: TLabel;
    edt4: TEdit;
    procedure btn1Click(Sender: TObject);
    procedure btn2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btn3Click(Sender: TObject);
    procedure chk1Click(Sender: TObject);
    procedure edt1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure ExecuteWait(const sProgramm: string; const sParams: string = ''; fHide: Boolean = false);
var
  ShExecInfo: TShellExecuteInfo;
begin
  FillChar(ShExecInfo, sizeof(ShExecInfo), 0);
  with ShExecInfo do
  begin
    cbSize := sizeof(ShExecInfo);
    fMask := SEE_MASK_NOCLOSEPROCESS;
    lpFile := PChar(sProgramm);
    lpParameters := PChar(sParams);
    lpVerb := 'open';
    if (not fHide) then
      nShow := SW_SHOW
    else
      nShow := SW_HIDE
  end;
  if (ShellExecuteEx(@ShExecInfo) and (ShExecInfo.hProcess <> 0)) then
    try
      WaitForSingleObject(ShExecInfo.hProcess, INFINITE)
    finally
      CloseHandle(ShExecInfo.hProcess);
    end;
end;

procedure TForm1.btn1Click(Sender: TObject);
var
  matchid: string;
  steamid: string;
  matchmode: string;
  List: TStringList;
  i: Integer;
begin
  if not chk1.Checked then
  begin
    //������� �� ������
    steamid := edt2.Text;
    if (pos('profiles/',steamid)<>0) then
    begin
      steamid := Copy(steamid,pos('profiles/',steamid)+9,17);
      edt2.Text := steamid;
    end;

    matchid := edt1.Text;
    if Pos('steam://',matchid)<>0 then
    begin
      matchid := Copy(matchid,Pos('CSGO-',matchid),35);
      matchmode := '0';
    end else if Pos('CSGO-',matchid)<>0 then
      matchmode := '0'
    else
      try
        StrToInt(Copy(matchid,1,5));
        StrToInt(Copy(matchid,5,5));
        StrToInt(Copy(matchid,10,5));
        matchmode := '1';
      except
        matchmode := '2';
      end;

    ShellExecute(0,nil,'CSGOBot.bat',PChar('0 ' + steamid + ' ' + matchmode + ' ' + matchid),'',1);
  end else
  begin
    // ������� �� ������
    DeleteFile('LOG.txt');
    List:=TStringList.Create;
    List.LoadFromFile('List.txt');
    for i:=0 to List.Count-1 do
    begin
      steamid := List.Strings[i];
      if (pos('profiles/',steamid)<>0) then
      begin
        steamid := Copy(steamid,pos('profiles/',steamid)+9,17);
      end;
      ExecuteWait('CSGOBot.bat',PChar('0 ' + steamid + ' ' + matchmode + ' ' + matchid + '>> LOG.txt'),false);
    end;
    List.Free
  end;
end;

procedure TForm1.btn2Click(Sender: TObject);
var
  steamid: string;
begin
  steamid := edt3.Text;

  ShellExecute(0,nil,'CSGOBot.bat',PChar('1 ' + steamid + ' 1 ' + edt4.Text),'',1);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  if not DirectoryExists('node_modules') then
  begin
    pgctab.Visible:=False;
    btn3.Visible:=True;
  end;
end;

procedure TForm1.btn3Click(Sender: TObject);
begin
  ShellExecute(0,nil,'npm','install','',1);
  Close;
end;

procedure TForm1.chk1Click(Sender: TObject);
begin
  if not chk1.Checked then
  begin
    edt1.Enabled := True;
    edt2.Enabled := True;
  end else
  begin
    edt1.Enabled := False;
    edt2.Enabled := False;
  end;
end;

procedure TForm1.edt1DblClick(Sender: TObject);
var
  MatchIDFile: TStringList;
  MatchID: string;
begin
  if (pos('profiles/',edt2.Text)<>0) then
  begin
    edt2.Text := Copy(edt2.Text,pos('profiles/',edt2.Text)+9,17);
  end;

  DeleteFile('playerMatchId.txt');
  ExecuteWait('node.exe','GetMatchID.js ' + edt2.Text);

  if FileExists('playerMatchId.txt') then
  begin
    MatchIDFile := TStringList.Create;
    MatchIDFile.LoadFromFile('playerMatchId.txt');
    MatchID := MatchIDFile.Strings[0];
    MatchIDFile.Free;
  end else MatchID := 'game_not_found';

  if (MatchID <> 'game_not_found') then
    edt1.Text := MatchID
  else
    edt1.Text := 'Game Not Found';
end;

end.
