var Steam = require("steam"),
    util = require("util"),
    fs = require("fs"),
    csgo = require("csgo"),
	Long = require("long"),
    bot = new Steam.SteamClient(),
    steamUser = new Steam.SteamUser(bot),
    steamFriends = new Steam.SteamFriends(bot),
    steamGC = new Steam.SteamGameCoordinator(bot, 730);
    CSGOCli = new csgo.CSGOClient(steamUser, steamGC, false),
    readlineSync = require("readline-sync"),
    crypto = require("crypto");
	SteamID64 = process.argv[2];

function MakeSha(bytes) {
    var hash = crypto.createHash('sha1');
    hash.update(bytes);
    return hash.digest();
}

var onSteamLogOn = function onSteamLogOn(response){
        if (response.eresult == Steam.EResult.OK) {
            console.log('Logged in!');
        }
        else
        {
            util.log('error, ', response);
            process.exit();
        }
        steamFriends.setPersonaState(Steam.EPersonaState.Busy);

        CSGOCli.launch();

        CSGOCli.on("unhandled", function(message) {
            util.log("Unhandled msg");
            util.log(message);
        });

        CSGOCli.on("ready", function() {
			CSGOCli.requestLiveGameForUser(CSGOCli.ToAccountID(String(SteamID64)));
			CSGOCli.on("matchList", function(list) {
			   if (list.matches && list.matches.length > 0) {
					var matchIDobj = JSON.parse(JSON.stringify(list.matches[0], null, 2));
					var matchID = new Long(matchIDobj.matchid.low, matchIDobj.matchid.high, matchIDobj.matchid.unsigned).toString();
					console.log("MatchID: ",matchID);
					fs.writeFile('playerMatchId.txt', matchID);
			   } else {
					console.log("Game not found!");
					fs.writeFile('playerMatchId.txt', "game_not_found");
			   }
			   
			   setTimeout(function() {process.exit();}, 2*1000);
			});


        });

        CSGOCli.on("unready", function onUnready(){
            util.log("node-csgo unready.");
        });

        CSGOCli.on("unhandled", function(kMsg) {
            util.log("UNHANDLED MESSAGE " + kMsg);
        });
    },
    onSteamSentry = function onSteamSentry(sentry) {
        util.log("Received sentry.");
        require('fs').writeFileSync('sentryMatchID', sentry);
    }

var username = "";
var password = "";
var authCode = "";

var logOnDetails = {
    "account_name": username,
    "password": password,
};
if (authCode !== "") {
    logOnDetails.auth_code = authCode;
}
var sentry = fs.readFileSync('sentryMatchID');
if (sentry.length) {
    logOnDetails.sha_sentryfile = MakeSha(sentry);
}
bot.connect();
steamUser.on('updateMachineAuth', function(response, callback){
    fs.writeFileSync('sentryMatchID', response.bytes);
    callback({ sha_file: MakeSha(response.bytes) });
});
bot.on("logOnResponse", onSteamLogOn)
    .on('sentry', onSteamSentry)
    .on('connected', function(){
        steamUser.logOn(logOnDetails);
    });
