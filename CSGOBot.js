//CSGOBot.js %0=report|1=commend% %SteamID64% %0=ShareURL|1=MatchID% %ShareUrl%

var fs = require("fs"),
    Steam = require("steam"),
    SteamID = require("steamid"),
    IntervalIntArray = {},
    readlineSync = require("readline-sync"),
    Protos = require("./protos/protos.js"),
    CountReports = 0,
    Long = require("long"),
    SteamClients = {},
    SteamUsers = {},
    SteamGCs = {},
	Long = require("long"),
    SteamFriends = {},
    process = require("process");
	BotMode = process.argv[2];
    steamID = process.argv[3];//readlineSync.question("SteamID64 which will be reported: "),
	MatchMode = process.argv[4];
	MatchID = 0;
	ShareURL = "";// = readlineSync.question("Enter Share URL: ");
	crypto = require('crypto');

	
if (MatchMode == 0) {	
	SharecodeDecoder = require("./helpers/sharecode.js").SharecodeDecoder;
	var scDecoder = new SharecodeDecoder(process.argv[5]);
	console.log("Sharecode: "+process.argv[5]);
	MatchID = scDecoder.decode().matchId;
} else if (MatchMode == 1) {
	MatchID = process.argv[5];
} else if (MatchMode == 2) {
	MatchID = 8;
}
	console.log("SteamID64: " + steamID);
	console.log("MatchID:   " + MatchID);
	console.log("\n");

var ClientHello = 4006,
    ClientWelcome = 4004;

var accounts = [];

function getSHA1(bytes) {
  var hash = crypto.createHash('sha1');
  hash.update(bytes);
  return hash.digest();
}

var arrayAccountsTxt = fs.readFileSync("accounts.txt").toString().split("\n");
for (i in arrayAccountsTxt) {
    var accInfo = arrayAccountsTxt[i].toString().trim().split(":");
    var username = accInfo[0];
    var password = accInfo[1];
	var useSentry = accInfo[2];
	var sentry = accInfo[3];
    accounts[i] = [];
    accounts[i].push({
        username: username,
        password: password,
		sentry: sentry,
		useSentry: useSentry
    });
}

arrayAccountsTxt.forEach(processSteamReport);

function processSteamReport(element, indexElement, array) {
    if(element != "") {
        var account = element.toString().trim().split(":");
        var account_name = account[0];
        var password = account[1];
		var useSentry = account[2];
		var authcode = account[3];
		
        SteamClients[indexElement] = new Steam.SteamClient();
        SteamUsers[indexElement] = new Steam.SteamUser(SteamClients[indexElement]);
        SteamGCs[indexElement] = new Steam.SteamGameCoordinator(SteamClients[indexElement], 730);
        SteamFriends[indexElement] = new Steam.SteamFriends(SteamClients[indexElement]);
        
        SteamClients[indexElement].connect();
		
		if (useSentry == 1) {
			var sentry = getSHA1(fs.readFileSync("sentry/" + account_name));
			SteamClients[indexElement].on("connected", function() {
				SteamUsers[indexElement].logOn({
					account_name: account_name,
					password: password,
					sha_sentryfile: sentry
				});
			});
		} else if (useSentry == 0) {
			SteamClients[indexElement].on("connected", function() {
				SteamUsers[indexElement].logOn({
					account_name: account_name,
					password: password,
					auth_code: authcode
				});
			});
			
			SteamUsers[indexElement].on('updateMachineAuth', function(sentry, callback) {
			  fs.writeFileSync("sentry/" + account_name, sentry.bytes);
			  callback({ sha_file: getSHA1(sentry.bytes) });
			}); 

		
		} else if (useSentry == -1) {
			SteamClients[indexElement].on("connected", function() {
				SteamUsers[indexElement].logOn({
					account_name: account_name,
					password: password
				});
			});
		}


        SteamClients[indexElement].on("logOnResponse", function(res) {
            if (res.eresult !== Steam.EResult.OK) {
                if (res.eresult == Steam.EResult.ServiceUnavailable) {
                    console.log("\n[STEAM CLIENT - " + account_name + "] Login failed - STEAM IS DOWN!");
                    console.log(res);
                    SteamClients[indexElement].disconnect();
                    process.exit();
                } else {
                    console.log("\n[STEAM CLIENT - " + account_name + "] Login failed!");
                    console.log(res);
                    SteamClients[indexElement].disconnect();
					SteamClients.splice(indexElement, 1);
                    SteamFriends.splice(indexElement, 1);
                    SteamGCs.splice(indexElement, 1);
                    SteamUsers.splice(indexElement, 1);
                    IntervalIntArray.splice(indexElement, 1);
                }
            } else {
                SteamFriends[indexElement].setPersonaState(Steam.EPersonaState.Offline);

                SteamUsers[indexElement].gamesPlayed({
                    games_played: [{
                        game_id: 730
                    }]
                });

                if (SteamGCs[indexElement]) {
                    IntervalIntArray[indexElement] = setInterval(function() {
                        SteamGCs[indexElement].send({
                            msg: ClientHello,
                            proto: {}
                        }, new Protos.CMsgClientHello({}).toBuffer());
                    }, 2000);
                } else {
                    SteamClients[indexElement].disconnect();
					SteamClients.splice(indexElement, 1);
                    SteamFriends.splice(indexElement, 1);
                    SteamGCs.splice(indexElement, 1);
                    SteamUsers.splice(indexElement, 1);
                    IntervalIntArray.splice(indexElement, 1);
                }
            }
        });

        SteamClients[indexElement].on("error", function(err) {
            console.log("[STEAM CLIENT - " + account_name + "] Account is probably ingame! Logged out!");
            SteamClients[indexElement].disconnect();
			SteamClients.splice(indexElement, 1);
            SteamFriends.splice(indexElement, 1);
            SteamGCs.splice(indexElement, 1);
            SteamUsers.splice(indexElement, 1);
            IntervalIntArray.splice(indexElement, 1);
        });

        SteamGCs[indexElement].on("message", function(header, buffer, callback) {
            switch (header.msg) {
                case ClientWelcome:
                    clearInterval(IntervalIntArray[indexElement]);
					if (BotMode == 0) {
						sendReport(SteamGCs[indexElement], SteamClients[indexElement], account_name, steamID);
					} else {
						sendCommend(SteamGCs[indexElement], SteamClients[indexElement], account_name, steamID);
					}
                    break;
                case Protos.ECsgoGCMsg.k_EMsgGCCStrike15_v2_MatchmakingGC2ClientHello:
                    break;
                case Protos.ECsgoGCMsg.k_EMsgGCCStrike15_v2_ClientReportResponse:
                    CountReports++;
					if (BotMode == 0) {
						console.log("[GC - " + account_name + "] (" + CountReports + ") Report with confirmation ID: " + Protos.CMsgGCCStrike15_v2_ClientReportResponse.decode(buffer).confirmationId.toString() + " sent!");
					} else {
						console.log("[GC - " + account_name + "] (" + CountReports + ") commend(s) successfully sent!");
					}
                    
                    SteamClients[indexElement].disconnect();
					SteamClients.splice(indexElement, 1);
                    SteamFriends.splice(indexElement, 1);
                    SteamGCs.splice(indexElement, 1);
                    SteamUsers.splice(indexElement, 1);
                    IntervalIntArray.splice(indexElement, 1);
                    break;
                default:
                    console.log(header);
                    break;
            }
        });
    }
}

function sendReport(GC, Client, account_name) {
    var account_id = new SteamID(steamID).accountid;
    GC.send({
        msg: Protos.ECsgoGCMsg.k_EMsgGCCStrike15_v2_ClientReportPlayer,
        proto: {}
    }, new Protos.CMsgGCCStrike15_v2_ClientReportPlayer({
        accountId: account_id,
        matchId: String(MatchID),
        rptAimbot: 2,
        rptWallhack: 3,
        rptSpeedhack: 4,
        rptTeamharm: 5,
        rptTextabuse: 6,
        rptVoiceabuse: 7
    }).toBuffer());
}

function sendCommend(GC, Client, account_name) {
    var account_id = new SteamID(steamID).accountid;
	GC.send({
        msg: Protos.ECsgoGCMsg.k_EMsgGCCStrike15_v2_ClientCommendPlayer,
        proto: {}
    }, new Protos.CMsgGCCStrike15_v2_ClientCommendPlayer({
        accountId: account_id,
        matchId: String(MatchID),
        commendation:  new Protos.PlayerCommendationInfo({
									cmdFriendly: 1,
									cmdTeaching: 2,
									cmdLeader: 4
								})

    }).toBuffer());
}

process.on("uncaughtException", function(err) {});

if (BotMode == 0) {
	console.log("Initializing Report Bot...");
} else {
	console.log("Initializing Commend Bot...");
}
